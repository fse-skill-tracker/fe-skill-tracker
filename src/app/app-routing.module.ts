import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchUserComponent } from './components/search-user/search-user.component';
import { LoginComponent } from './components/login/login.component'


const routes: Routes = [
  {
    path:'search-user', component: SearchUserComponent
    
  },
  {
    path:'login', component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
