import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchUserService {


  baseURL = "http://apigateway-env.eba-imrx3tjr.us-east-1.elasticbeanstalk.com/skill-tracker/api/v1/admin/";
  constructor(private http: HttpClient) { }

  searchUserByName(user: any):Observable<any> {
    return this.http.get(this.baseURL+"name/"+user);
  }

  searchUserByAssociateId(associateID: any):Observable<any> {
    return this.http.get(this.baseURL+"associateId/"+associateID);
  }

  searchUserBySkill(skill: any):Observable<any> {
    return this.http.get(this.baseURL+"skill/"+skill);
  }
}
