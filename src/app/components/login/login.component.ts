import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    loginName : new FormControl('', Validators.required),
    loginPassword : new FormControl('')
  });
  constructor(private _router: Router) { }

  ngOnInit(): void {
  }

  login(){
    if(this.loginForm.value.loginName === 'admin' 
    &&  this.loginForm.value.loginPassword === 'password'){
      this._router.navigateByUrl('/search-user');
    }else{
      this.loginForm.setErrors({incorrectUser:true});
    }
    
    

  }

}
