import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SkillTracker } from 'src/app/model/skill-tracker.model';
import { SearchUserService } from 'src/app/service/search-user.service';


@Component({
  selector: 'app-search-user',
  templateUrl: './search-user.component.html',
  styleUrls: ['./search-user.component.scss']
})
export class SearchUserComponent implements OnInit {
   
  constructor(private searchService: SearchUserService) {
   
   }
  errorMessage:string = "";
  searchedData: SkillTracker[] = [];
  skills = [
    {type:"HTML-CSS-JavaScript", value:"htmlJs"},
    {type:"Angular", value:"angular"},
    {type:"React", value:"react"},
    {type:"SpringBoot", value:"spring"},
    {type:"Restful", value:"restful"},
    {type:"Hibernate", value:"hibernate"},
    {type:"GIT", value:"git"},
    {type:"Docker", value:"docker"},
    {type:"Jenkins", value:"jenkins"},
    {type:"AWS", value:"aws"},
  ]
   
  searchUserForm = new FormGroup({
    userName : new FormControl(''),
    associateId : new FormControl(''),
    skill : new FormControl('')
  });
  ngOnInit(): void {
  }

  searchByName(){
    this.clearFormErrors();
    console.log("search by name" + this.searchUserForm.value.userName);
    this.searchedData = [];
    const user = this.searchUserForm.value.userName;
    if(user != undefined && user != '' ){
      this.searchService.searchUserByName(user).subscribe((data: SkillTracker[])=>
      {
        console.log(data);
        this.searchUserForm.reset();
        this.searchedData = data;
      }, err => {
        console.log(err.error.message);
        this.errorMessage = err.error.message;
        this.searchUserForm.setErrors({nodata:true});

      });

    }else{
      this.searchUserForm.controls['userName'].setErrors({invalid:true});
    }

  }

  searchByAssociateId(){
    this.clearFormErrors();
    const associateId = this.searchUserForm.value.associateId;
    console.log("search by associate" + associateId);
    this.searchedData = [];
    if(associateId != undefined && associateId!='' ){
      this.searchService.searchUserByAssociateId(associateId).subscribe((data)=>
      {
        console.log(data);
        this.searchUserForm.reset();
        this.searchedData = data;
      }, err => {
        console.log(err.error.message);
        this.errorMessage = err.error.message;
        this.searchUserForm.setErrors({nodata:true});
      });
    }else{
      this.searchUserForm.controls['associateId'].setErrors({invalid:true});
    }
  }

  searchBySkill(){
    this.clearFormErrors();
    const skill = this.searchUserForm.value.skill;
    console.log("search by skill"+skill);
    this.searchedData = [];
    if(skill != undefined && skill!=''){
      this.searchService.searchUserBySkill(skill).subscribe(
        (data) => {
        console.log(data);
        this.searchUserForm.reset();
        this.searchedData = data;
      }, err => {
        console.log(err.error.message);
        this.errorMessage = err.error.message;
        this.searchUserForm.setErrors({nodata:true});

      });
    }else{
      this.searchUserForm.controls['skill'].setErrors({invalid:true});
    }

  }

  clearFormErrors(){
    Object.keys(this.searchUserForm.controls).forEach((key) => {
      const control = this.searchUserForm.controls[key];
      control.setErrors(null);
      control.updateValueAndValidity();
  });
  this.searchUserForm.setErrors(null);
  }


}
