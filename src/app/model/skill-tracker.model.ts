import { SoftSkill } from "./soft-skill.model";
import { TechSKill } from "./tech-skill.model";
import { UserData } from "./user-data.model";

export interface SkillTracker{
    userProfile: UserData,
    techSkills: TechSKill,
    softSkills: SoftSkill
}