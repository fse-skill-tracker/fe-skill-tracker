export interface SoftSkill{
    id: string,
    associateId: String,
    spoken: number,
    communication: number,
    aptitude: Number
} 