export interface UserData{
    id: String,
    associateId: String,
    name: String,
    email: String,
    mobile: String
}