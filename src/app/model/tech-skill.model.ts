export interface TechSKill{
    id: string,
    associateId: String,
    htmlJS: number,
    angular: number,
    react: number,
    spring: number,
    restful: number,
    hibernate: number,
    git: number,
    docker: number,
    jenkins: number,
    aws: number
}